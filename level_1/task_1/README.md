## Level 1 Activities

# Task 1 - Simple supply

## Description: Slowly introduce the students to working with the LEGOS Demonstrator, Duration: ~ 10 min
- A) Establish a connection between a source and a load
- B) What is the relationship between current, voltage and resistance?
- C) Does a current flow when the consumer is not connected? If so, how large is this?
- D) Calculate the voltage at the load
- E) What applies to the power at the consumer? What does the power depend on?
